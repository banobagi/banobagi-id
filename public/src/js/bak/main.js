$(document).ready(function(){
	$(window).on("load resize", function () {
		$('body').attr('data-mobile',
			(function(){
				var r = ($(window).width() <= 992) ? true : false;
				return r;
			}())
		);
		$('.main__section').css('height','100vh');
	});

	var f;
	var url;
	$(document).ready(function($){
		f = $("#vimeo_main_iframe");
		if(!f || !f.attr("src") ) return;
	    url = f.attr("src").split("?")[0];
	});


	$(window).load(function(){
		$.scrollify({
			section : ".main__section",
			interstitialSection : ".footer",
			scrollSpeed: 700,
			scrollbars: false,
			touchScroll:true,
			setHeights:true,
			before: function(i,panels) {
	 			var ref = panels[i].attr("data-section-name");
				$(".pagination .active").removeClass("active");
      			$(".pagination").find("a[href=\"#" + ref + "\"]").addClass("active");
				if(ref==="section1") {
					$('.pagination').hide();
					var data = {
						method: "play",
						value: 1
					};
					f[0].contentWindow.postMessage(JSON.stringify(data), url);
				}else{
					$('.pagination').show();
				}

				if(ref==="section2") {
					var data = {
						method: "pause",
				        value: 1
				    };
				    f[0].contentWindow.postMessage(JSON.stringify(data), url);
					$(".js-img1").addClass("is-move");
					$(".js-img2, .js-img3, .js-img4, .js-img5").removeClass("is-move");
				}else if(ref==="section3") {
        			$(".js-img2").addClass("is-move");
					$(".js-img1, .js-img3, .js-img4, .js-img5").removeClass("is-move");
				}else if(ref==="section4"){
					$(".js-img3").addClass("is-move");
					$(".js-img1, .js-img2, .js-img4, .js-img5").removeClass("is-move");
				}else if(ref==="section5"){
					$(".js-img4").addClass("is-move");
					$(".js-img1, .js-img2, .js-img3, .js-img5").removeClass("is-move");
				}else if(ref==="section6"){
					$(".js-img5").addClass("is-move");
					$(".js-img1, .js-img2, .js-img3, .js-img4").removeClass("is-move");
				}else{
					$(".js-img1, .js-img2, .js-img3, .js-img4, .js-img5").removeClass("is-move");
				}
			},
			afterRender:function() {
		      var pagination = "<ul class=\"pagination\">";
		      var activeClass = "";
		      $(".page-section").each(function(i) {
		        activeClass = "";
		        if(i===0) {
		          activeClass = "active";
		        }
		        pagination += "<li class='pagination__item'><a class=\"" + activeClass + " pagination__link\" href=\"#" + $(this).attr("data-section-name") + "\"></a></li>";
		      });

		      pagination += "</ul>";

		      $(".main").append(pagination);
		      $(".pagination a").on("click",$.scrollify.move);
		  },afterResize:function() {
			  $.scrollify.update()
			  $('.main__section').css('height','100vh');
		  }
		});
		$('.js-down').on('click',function(){
			$.scrollify.move("#section2");
		});
		var cnt = 1;
		$('.js-menu').on('click',function(){
			if(cnt == 1){
				$.scrollify.disable();
				return cnt = 0;
			}else{
				$.scrollify.enable();
				return cnt = 1;
			}
		});
		$('.js-popup-open').on('click',function(){
			$.scrollify.disable();
		});
		$('.js-popup-close').on('click',function(){
			if(cnt == 0){
				return false;
			}
			$.scrollify.enable();
		});
	});
	$('.main__item').on('mouseenter',function(){
		if ($('body').attr('data-mobile') == 'false'){
			$(this).find('.main__link, .main__item-img').addClass('is-active');
		}
	}).on('mouseleave',function(){
		if ($('body').attr('data-mobile') == 'false'){
			$(this).find('.main__link, .main__item-img').removeClass('is-active');
		}
	});
	$('.main__item').on('click',function(){
		if ($('body').attr('data-mobile') == 'true'){
			$(this).find('.main__link, .main__item-img').toggleClass('is-active');
			$(this).siblings().find('.main__link, .main__item-img').removeClass('is-active');
		}
	})
});
