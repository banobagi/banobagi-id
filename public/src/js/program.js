$(document).ready(function(){
	$('.js-top').click(function(e){
		e.preventDefault();
		$('html,body').animate({'scrollTop':'0'},300);
	});

	$('.visual__list').cycle({
		slides:'.visual__item',
		fx:'scrollHorz',
		loop:false,
		timeout:4000,
		swipe:true,
		pager:'.visual__pager',
		pagerTemplate:'<span class="visual__bull">&bull;</span>',
		pagerActiveClass:'is-active'
	});
});
